﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cnsChessBoard
{
    internal class Program
    {
        static char[] letters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };

        private static void Main()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    // Bottom
                    if (i > 1 && j > 1)
                    {
                        if (i % 2 == 0)
                        {
                            if (j % 2 == 0)
                            {
                                Console.Write("  ");
                            }
                            else
                            {
                                Console.Write("■ ");
                            }
                        }
                        else
                        {
                            if (j % 2 == 1)
                            {
                                Console.Write("  ");
                            }
                            else
                            {
                                Console.Write("■ ");
                            }
                        }
                        
                        
                    }
                    else if ((i >= 0 && j >= 0) && (i <= 1))
                    {

                        if (i > 0 && j < 10)
                        {
                            Console.Write("_ ");

                        }
                        else if (i == 0 && j < 10)
                        {
                            if (j < 2)
                            {
                                Console.Write("  ");
                            }

                            if (j > 1 && j != 0)
                            {
                                
                                Console.Write($"{j-1} ");
                            }
                        }
                        else
                        {
                            Console.Write("_");
                        }
                    }

                    // Top
                    if ((i > 1 && j >= 0) && (j <= 1))
                    {
                        if (i > 1 && j == 0)
                        {
                            Console.Write($"{letters[i-2]} ");
                        }
                        else
                        {
                            Console.Write("| ");
                        }

                    }


                }

                Console.WriteLine();
            }
        }
    }
}

